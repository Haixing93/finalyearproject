# FinalYearProject

To design a servo system with laser mounted, and able to integrate with an image processing system.

# Setup

1.  Mount the camera using the bolt
2.  Connects the camera to UP board using a USB Type-C cable
3.  Connects the signal pin of the pan motor to pin 32 on the UP board
4.  Connects the signal pin of the tilt motor to pin 33 on the UP board
5.  Mount the laser onto the system
6.  Connects the base of the transistor to pin 5 on the UP board
7.  Connects the positive end of the laser to 3V3 pin on the UP board
8.  Connects the emitter of the transistor to a ground pin on the UP board
9.  Power up the UP board
10. Run the companion.py file

# Documents

1. [Servo motor data sheet](http://www.ee.ic.ac.uk/pcheung/teaching/DE1_EE/stores/sg90_datasheet.pdf)
2. [UP squared data sheet](https://www.up-board.org/wp-content/uploads/2017/11/UP-Square-DatasheetV0.5.pdf)

# License

[MIT](https://gitlab.com/Haixing93/finalyearproject/blob/master/LICENSE)